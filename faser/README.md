Fork the FASER GenieGenerator repository to a GitLab project of your own.

Use your GitLab name in the `git clone` command.

You can name your working branch anything (`genie-working-branch` is just an example) but it should be unique

```
mkdir genie
cd genie
git clone https://:@gitlab.cern.ch:8443/[your name]/GenieGenerator.git
cd GenieGenerator
git remote add upstream https://:@gitlab.cern.ch:8443/faser/GenieGenerator.git
git fetch --tags upstream
git checkout -b genie-working-branch upstream/faser-R-3_00_06 --no-track
source faser/setupGenerator.sh
source faser/buildGenerator.sh

source faser/getXsec.sh

cd ../run
source ../GenieGenerator/faser/runGenerator_1.sh 
source ../GenieGenerator/faser/runGenerator_2.sh
```

Note that the cross-section file only needs to be downloaded once using getXsec.sh


