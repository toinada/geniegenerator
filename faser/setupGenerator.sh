#!/bin/bash

if [[ -n "$1" ]]; then
    SYS_CONFIG=$1
else
    SYS_CONFIG="x86_64-centos7-gcc8-opt"
fi

asetup master,latest,Athena

ROOT_VERSION=6.14.00-5074b
source /cvmfs/sft.cern.ch/lcg/releases/ROOT/$ROOT_VERSION/$SYS_CONFIG/bin/thisroot.sh

GSL_VERSION=2.5-32fc5
GSL_PATH=/cvmfs/sft.cern.ch/lcg/releases/GSL/$GSL_VERSION/$SYS_CONFIG

PYTHIA6_VERSION=429.2-c4089
export PYTHIA6_PATH=/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/pythia6/$PYTHIA6_VERSION/$SYS_CONFIG

LHAPDF6_VERSION=6.2.3-ff12d
export LHAPDF6_PATH=/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/lhapdf/$LHAPDF6_VERSION/$SYS_CONFIG

LOG4CPP_VERSION=2.8.3p1-72f76
LOG4CPP_PATH=/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/log4cpp/$LOG4CPP_VERSION/$SYS_CONFIG

LIBXML2_VERSION=2.9.9-3501e
LIBXML2_PATH=/cvmfs/sft.cern.ch/lcg/releases/libxml2/$LIBXML2_VERSION/$SYS_CONFIG

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
export GENIE_HOME=$SCRIPT_DIR/../..
export GENIE=$SCRIPT_DIR/..

export PATH=$PATH:\
$ROOTSYS/bin:\
$GSL_PATH/bin:\
$GENIE_HOME/run/bin

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:\
$GSL_PATH/lib:\
$PYTHIA6_PATH/lib:\
$LHAPDF6_PATH/lib:\
$LOG4CPP_PATH/lib:\
$LIBXML2_PATH/lib:\
$GENIE_HOME/run/lib

