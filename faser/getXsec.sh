#!/bin/bash

pushd $GENIE_HOME

wget https://scisoft.fnal.gov/scisoft/packages/genie_xsec/v3_00_06/genie_xsec-3.00.06-noarch-G1802a00000-k250-e1000.tar.bz2
tar xvf genie_xsec-3.00.06-noarch-G1802a00000-k250-e1000.tar.bz2

popd
