#!/bin/bash

cd $GENIE_HOME/run

# Example in manual has multiple syntax errors, and flux histogram file doesn't exist

gevgen -n 10000 -e 0,10 -p 14 -t 1000080160 -r 100 -f $GENIE/data/flux/t2ksk.root,numu_flux --seed 2989819 --cross-sections $GENIE_HOME/genie_xsec/v3_00_06/NULL/G1802a00000-k250-e1000/data/gxspl-FNALsmall.xml

 
