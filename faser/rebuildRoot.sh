#!/bin/bash

# This script rebuilds a private version of ROOT with Pythia6 and Mathmore support.
# You should not use it in the Genie directory, but copy it somewhere else where you want to rebuild ROOT.
# To use the version of ROOT generated, you will need to modify the setupGenerator.sh script to point to
# to the private installation directory rather than CVMFS.

ROOT_VERSION=v6-18-04

asetup cmakesetup,gcc8,none

mkdir ROOT
cd ROOT

mkdir build
git clone http://github.com/root-project/root.git
cd root
git checkout -b $ROOT_VERSION $ROOT_VERSION
cd ../build

export PYTHIA6_DIR=/cvmfs/sft.cern.ch/lcg/releases/MCGenerators/pythia6/429.2-c4089/x86_64-centos7-gcc8-opt/lib/
# Next two lines may be unneeded
export GSL_LIBRARY=/cvmfs/sft.cern.ch/lcg/releases/GSL/2.5-32fc5/x86_64-centos7-gcc8-opt/lib
export GSL_INCLUDE=/cvmfs/sft.cern.ch/lcg/releases/GSL/2.5-32fc5/x86_64-centos7-gcc8-opt/include

cmake -Dpythia6=ON -Dmathmore=ON -DCMAKE_INSTALL_PREFIX=../run -DGSL_ROOT_DIR=/cvmfs/sft.cern.ch/lcg/releases/GSL/2.5-32fc5/x86_64-centos7-gcc8-opt/ -DGSL_CONFIG_EXECUTABLE=/cvmfs/sft.cern.ch/lcg/releases/GSL/2.5-32fc5/x86_64-centos7-gcc8-opt/bin -Dbuiltin_fftw3=ON -Dbuiltin_cfitsio=ON -Dbuiltin_xrootd=ON -Dbuiltin_davix=ON ../root/

cmake --build . --target install
