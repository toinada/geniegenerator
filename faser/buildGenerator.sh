#!/bin/bash

cd $GENIE
./configure --prefix=$GENIE/../run --with-pythia6-lib=$PYTHIA6_PATH/lib --disable-lhapdf5 --enable-lhapdf6 --with-lhapdf6-lib=$LHAPDF6_PATH/lib --with-lhapdf6-inc=$LHAPDF6_PATH/inc
gmake
mkdir -p $GENIE/../run/bin
mkdir -p $GENIE/../run/lib
mkdir -p $GENIE/../run/include
gmake install

